<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rawilk\Printing\Facades\Printing;

class PrintingController extends Controller
{
    public function index()
    {
        $printers = Printing::printers();
        dd($printers);
        foreach ($printers as $printer) {
            echo $printer->id()."<br>";
        }
    }

    public function print(){
        Printing::newPrintTask()
            ->printer('ipp://127.0.0.1:631/printers/PRINTER26')
            ->url('http://localhost/rsibyl/file/ekg/2366_001529_1637555358.pdf')
            ->send();
    }
}
